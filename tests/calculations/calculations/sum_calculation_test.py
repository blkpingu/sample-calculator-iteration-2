# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Implements the sum calculation tests.
"""


import decimal

from sample_calculator.calculations.calculations import sum_calculation


class TestSumCalculation:
    def setup_method(self, _):
        self._sum = sum_calculation.SumCalculation(list(), dict())

    def test_calculate_success(self):
        assert self._sum.calculate([decimal.Decimal(1), decimal.Decimal(2), decimal.Decimal(3)]) == 6

    def test_calculate_empty(self):
        assert self._sum.calculate([]).is_nan()
